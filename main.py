from flask import render_template, request

from create_db import app, db, Book, Author, Publisher, create_books



@app.route('/')
def index():
	return render_template('index.html')


@app.route('/books/<int:page_num>')
def books(page_num):
    books = db.session.query(Book).paginate(per_page=5, page=page_num, error_out=False)
    return render_template('Books.html', books=books)


@app.route('/authors/<int:page_num>')
def authors(page_num):
    books = db.session.query(Author).paginate(per_page=5, page=page_num, error_out=False)
    return render_template('Authors.html', books=books)

@app.route('/publishers/<int:page_num>')
def publishers(page_num):
    books = db.session.query(Publisher).paginate(per_page=5, page=page_num, error_out=False)
    return render_template('Publishers.html', books=books)

@app.route('/about/')
def about():
    return render_template('About.html')

if __name__ == "__main__":
	app.run()

