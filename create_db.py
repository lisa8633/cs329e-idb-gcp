# beginning of create_db.py
import json
from models import app, db, Book, Author, Publisher

# load json file
def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn


def create_books():
    book = load_json('books.json')

    for oneBook in book['Books']:
        # book attributes
        title = oneBook['title']
        id = oneBook['id']
        year= oneBook['year']
        image= oneBook['image_url']
        summary= oneBook['summary']

        # author attributes
        author = oneBook['author']
        birthday = oneBook['born']
        author_image= oneBook['author_url']
        bio = oneBook['bio']
        books_written= oneBook['books written']
        nationality= oneBook['nationality']

        # publisher attributes
        publisher = oneBook['publisher']
        description= oneBook['description']
        books_published= oneBook['books published']
        website= oneBook['website']
        publisher_image= oneBook['publisher_url']
        owner= oneBook['owner']
        unique= oneBook['unique']
        # add to Author is not duplicate
        if unique=='0' or unique=='2':
            newAuthor= Author(name=author, birthday=birthday, author_image= author_image, bio=bio, nationality= nationality, books_written=books_written)
            db.session.add(newAuthor)
        # add to Publisher is not duplicate
        if unique=='0' or unique=='1':
            newPublisher= Publisher(publisher=publisher, description=description, website=website, books_published= books_published, owner=owner, publisher_image= publisher_image)
            db.session.add(newPublisher)
        # add to Book
        newBook = Book(title=title, id=id, author=author, year=year, publisher=publisher, image=image, summary=summary,unique=unique)
        # After I create the book, I can then add it to my session.
        db.session.add(newBook)

        # commit the session to my DB.
        db.session.commit()

create_books()
# end of create_db.py
