import os
import sys
import unittest
#from models import db, Bo
from create_db import db, Book, Author, Publisher

# Unite Test
class DBTestCases(unittest.TestCase):
    # unit tests for Books
    def test_source_insert_1(self):
        # precondition
        r = db.session.query(Book).first()
        assert r != None
        s = Book(title='Foundation and Empire', id='100', author='Isaac Asimov', year='2004', publisher='Bantam Spectra', image='http://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Isaac.Asimov01.jpg/220px-Isaac.Asimov01.jpg', summary='Although small and seemingly helpless, the Foundation had managed to survive against the greed of its neighboring warlords. But could it stand against the mighty power of the Empire, who had created a mutant man with the strength of a dozen battlefleets...? From the Paperback edition')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(id = '100').first()
        self.assertEqual(str(r.id), '100')
        # post condition
        db.session.query(Book).filter_by(id = '100').delete()
        db.session.commit()

    def test_source_insert_2(self):
        # precondition
        r = db.session.query(Book).first()
        assert r != None
        s = Book(title='Becoming', id='99', author='Michelle Obama', year='2018', publisher='Crown Publishing Group', image='https://images-na.ssl-images-amazon.com/images/I/81h2gWPTYJL.jpg', summary='In her memoir, Michelle Obama takes readers through her life journey, starting from the very beginning, her childhood on the South Side of Chicago. We get introduced to her family and friends, hear about her struggles with the society and racial atmosphere at the time, and become familiar with all the stories that shaped her upbringing. Obama also writes about attending Princeton University, and how she dealt with attending a mostly white institution, and then eventually leaving her career as a lawyer. She lets readers in on the details of her marriage, such as her time in premarital counseling, and her trouble with infertility. Towards the end of the book, Michelle Obama focuses in on the challenges that she dealt with while being the First Lady and still successfully fulfilling her duty as a mother of two children.')
        db.session.add(s)
        db.session.commit()
    
    
        r = db.session.query(Book).filter_by(author = 'Michelle Obama').first()
        self.assertEqual(str(r.author), 'Michelle Obama')
        # post condition
        db.session.query(Book).filter_by(author = 'Michelle Obama').delete()
        db.session.commit()    

    def test_source_insert_3(self):
        # precondition
        r = db.session.query(Book).first()
        assert r != None
        s = Book(title='Royal Assassin', id='98', author='Robin Hobb', year='2002', publisher='Bantam Spectra', image='https://books.google.com/books/content/images/frontcover/dZNonS5wCOwC?fife=w500', summary='Fitz is a trained assassin in the service of King Shrewd and also the kings illegitimate grandson. He is sworn to protect heir to the throne Prince Verity and Veritys new bride, but his task is complicated by an invasion of vicious barbarians who turn helpless captives into zombie-like Forged Ones.')
        db.session.add(s)
        db.session.commit()
    
    
        r = db.session.query(Book).filter_by(year = '2002').first()

        self.assertEqual(str(r.year), '2002')
        self.assertEqual(str(r.title), "Royal Assassin")
        # post condition
        db.session.query(Book).filter_by(year = '2002').delete()
        db.session.commit()



    # Unite tests for Author
    def test_source_insert_author(self):
        # precondition
        r = db.session.query(Author).first()
        assert r != None
        s = Author( name='Harper-Lee', birthday='1926-04-28', author_image= 'https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTE4MDAzNDEwNTk2MjM0NzY2/harper-lee-9377021-1-402.jpg', bio='Nelle Harper Lee, better known by her pen name Harper Lee, was an American novelist widely known for To Kill a Mockingbird, published in 1960. Immediately successful, it won the 1961 Pulitzer Prize and has become a classic of modern American literature.', nationality= 'American', books_written='To Kill a Mockingbird, Go Set a Watchman')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Author).filter_by(name = 'Harper-Lee').first()
        self.assertEqual(str(r.name), 'Harper-Lee')
        #post condition

        db.session.query(Author).filter_by(name= 'Harper-Lee').delete()
        db.session.commit()

    def test_source_insert_author2(self):
        # precondition
        r = db.session.query(Author).first()
        assert r != None
        s = Author( name='Michelle-Obama', birthday='1964-01-17', author_image= 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Michelle_Obama_2013_official_portrait.jpg/220px-Michelle_Obama_2013_official_portrait.jpg', bio='Michelle LaVaughn Obama, an American lawyer, university administrator and writer, who was also the First Lady of the United States from 2008 to 2016.', nationality= 'American', books_written='American Grown: The Story of the White House Kitchen Garden and Gardens Across America, Michelle Obama: In Her Own Words, Becoming')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Author).filter_by(birthday='1964-01-17').first()
        self.assertEqual(str(r.birthday), '1964-01-17')

        db.session.query(Author).filter_by(birthday='1964-01-17').delete()
        db.session.commit()

    def test_source_insert_author3(self):
        # precondition
        r = db.session.query(Author).first()
        assert r != None
        s = Author( name='Jane-Austen', birthday='1775-12-16', author_image= 'https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTE1ODA0OTcxNTQ2ODcxMzA5/jane-austen-9192819-1-402.jpg', bio='Jane Austen was an English novelist known for her six major novels, which interpret, critique and comment upon the British landed gentry at the end of 18th century.', nationality= 'British', books_written='Pride and Prejudice, Sense and Sensibility, Mansfield Park, Emma')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Author).filter_by(name = 'Jane-Austen').first()
        self.assertEqual(str(r.name), 'Jane-Austen')

        db.session.query(Author).filter_by(name= 'Jane-Austen').delete()
        db.session.commit()



    # unite tests for Publishers
    def test_source_insert_publisher(self):
        # precondition
        r = db.session.query(Publisher).first()
        assert r!= None
        s= Publisher(publisher="Victor-Gollancz-Ltd", description='Victor Gollancz Ltd was a major British book publishing house of the twentieth century. It was founded in 1927 by Victor Gollancz and specialised in the publication of high quality literature, nonfiction and popular fiction, including crime, detective, mystery, thriller and science fiction.', website='http://www.gollancz.co.uk', books_published= 'About a Boy, The Birds and Other Stories, Before the Fact, Beowulf’s Children', owner='Orion-Publishing-Group', publisher_image= 'http://upload.wikimedia.org/wikipedia/en/thumb/1/1a/Victor-gollancz-logo.png/100px-Victor-gollancz-logo.png')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Publisher).filter_by(owner='Orion-Publishing-Group').first()
        self.assertEqual(str(r.owner), 'Orion-Publishing-Group')
        # post condition
        db.session.query(Publisher).filter_by(owner='Orion-Publishing-Group').delete()
        db.session.commit()

    def test_source_insert_publisher2(self):
        # precondition
        r = db.session.query(Publisher).first()
        assert r != None
        s= Publisher(publisher="Pottermoree", description='Pottermore is the digital publishing, e-commerce, entertainment, and news company from J.K. Rowling and is the global digital publisher of Harry Potter and J.K. Rowlings Wizarding World.', website='https://en.wikipedia.org/wiki/Pottermore', books_published= 'Harry Potter Series', owner='J. K. Rowling', publisher_image= 'https://images.pottermore.com/bxd3o8b291gf/20uhLB7Y4QE6EEQiieiegi/6013228e45148e5e0208008cd5fa7ca0/pottermore-wizarding-world-logo.jpg?w=1200')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Publisher).filter_by(publisher = 'Pottermoree').first()
        self.assertEqual(str(r.publisher), 'Pottermoree')
        # post condition
        db.session.query(Publisher).filter_by(publisher= 'Pottermoree').delete()
        db.session.commit()

    def test_source_insert_publisher3(self):
        # precondition
        r = db.session.query(Publisher).first()
        assert r != None
        s= Publisher(publisher="Harper-Collins", description='HarperCollins Publishers LLC is one of the worlds largest publishing companies and is part of the \"Big Five\" English-language publishing companies, alongside Hachette, Holtzbrinck/Macmillan, Penguin Random House, and Simon &amp; Schuster.', website='http://harpercollins.com', books_published= 'Harper Lee: To Kill a Mocking Bird, Marc Brandel: The Mine of Lost Days, John Rowe Townsend: The Creatures', owner='News Corp', publisher_image= 'http://www.stm-publishing.com/wp-content/uploads/2011/11/harper-1180x518_c.png')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Publisher).filter_by(publisher = 'Harper-Collins').first()
        self.assertEqual(str(r.publisher), 'Harper-Collins')
        # post condition
        db.session.query(Publisher).filter_by(publisher= 'Harper-Collins').delete()
        db.session.commit()





    # def test_source_insert_2(self):
    #     s = Book(title='Becoming', id='99', author='Michelle Obama', year='2018', publisher='Crown Publishing Group', image='https://images-na.ssl-images-amazon.com/images/I/81h2gWPTYJL.jpg', summary='In her memoir, Michelle Obama takes readers through her life journey, starting from the very beginning, her childhood on the South Side of Chicago. We get introduced to her family and friends, hear about her struggles with the society and racial atmosphere at the time, and become familiar with all the stories that shaped her upbringing. Obama also writes about attending Princeton University, and how she dealt with attending a mostly white institution, and then eventually leaving her career as a lawyer. She lets readers in on the details of her marriage, such as her time in premarital counseling, and her trouble with infertility. Towards the end of the book, Michelle Obama focuses in on the challenges that she dealt with while being the First Lady and still successfully fulfilling her duty as a mother of two children.',
    #                    description='The Crown Publishing Group is a subsidiary of Random House that publishes across several categories including fiction, non-fiction, biography, autobiography and memoir, cooking, health, business, and lifestyle.', website='http://crownpublishing.com/', books_published= 'Charles Krauthammer: The Point of It All, Andrea Bartz: The Lost Night, Rshma Saujani: Brave, Not Perfect', owner='Penguin Random House', publisher_url= 'http://assets.penguinrandomhouse.com/wp-content/uploads/2015/04/01075755/RH_Publishing_Services_logo_bw.jpg')
    #     db.session.add(s)
    #     db.session.commit()
    #
    #
    #     r = db.session.query(Book).filter_by(id = '99').one()
    #     self.assertEqual(str(r.id), '99')
    #     db.session.query(Book).filter_by(id = '99').delete()
    #     db.session.commit()
    #
    # def test_source_insert_3(self):
    #     s = Book(title='Royal Assassin', id='98', author='Robin Hobb', year='2002', publisher='Bantam Spectra', image='https://books.google.com/books/content/images/frontcover/dZNonS5wCOwC?fife=w500', summary='Fitz is a trained assassin in the service of King Shrewd and also the kings illegitimate grandson. He is sworn to protect heir to the throne Prince Verity and Veritys new bride, but his task is complicated by an invasion of vicious barbarians who turn helpless captives into zombie-like Forged Ones.',
    #                    description='Bantam Spectra is the science-fiction division of Bantam Books, which is owned by Random House.\nAccording to their website, Spectra publishes \"science-fiction, fantasy, horror, and speculative novels from recognizable authors\"', website='http://sf-fantasy.suvudu.com', books_published= 'Magician: Apprentice, The Soul of Power, The Murderer’s Memories', owner='Bantam Books/Random House', publisher_url= 'https://vignette.wikia.nocookie.net/starwars/images/9/9f/BantamSpectra.png/revision/latest?cb=20061121124259')
    #     db.session.add(s)
    #     db.session.commit()
    #
    #
    #     r = db.session.query(Book).filter_by(id = '98').one()
    #     self.assertEqual(str(r.id), '98')
    #
    #     db.session.query(Book).filter_by(id = '98').delete()
    #     db.session.commit()
    #
    # def test_source_insert_4(self):
    #     s = Book(title='Foundation and Empire', id='97', author='Isaac Asimov', year='2004', publisher='Bantam Spectra', image='http://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Isaac.Asimov01.jpg/220px-Isaac.Asimov01.jpg', summary='Although small and seemingly helpless, the Foundation had managed to survive against the greed of its neighboring warlords. But could it stand against the mighty power of the Empire, who had created a mutant man with the strength of a dozen battlefleets...? From the Paperback edition',
    #                    description='Bantam Spectra is the science-fiction division of Bantam Books, which is owned by Random House.\nAccording to their website, Spectra publishes \"science-fiction, fantasy, horror, and speculative novels from recognizable authors\".', website='http://sf-fantasy.suvudu.com', books_published= 'Magician: Apprentice, The Soul of Power, The Murderer’s Memories', owner='Bantam Books/Random House', publisher_url= 'https://vignette.wikia.nocookie.net/starwars/images/9/9f/BantamSpectra.png/revision/latest?cb=20061121124259')
    #     db.session.add(s)
    #     db.session.commit()
    #
    #
    #     r = db.session.query(Book).filter_by(id = '97').one()
    #     self.assertEqual(str(r.id), '97')
    #
    #     db.session.query(Book).filter_by(id = '97').delete()
    #     db.session.commit()

if __name__ == '__main__':
    unittest.main()
