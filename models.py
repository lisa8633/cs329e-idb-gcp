# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgres://postgres:lxy82699@localhost:5432/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # to suppress a warning message
db = SQLAlchemy(app)

class Book(db.Model):
    # Book table
    __tablename__ = 'book'

    title = db.Column(db.String(80), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    author= db.Column(db.String(80), nullable=False)
    year = db.Column(db.String(80), primary_key=False)
    publisher= db.Column(db.String(80), nullable=False)
    image = db.Column(db.String(120), nullable=False)
    unique = db.Column(db.Integer, primary_key=False)
    summary = db.Column(db.String(), nullable=False)

class Author(db.Model):
    # Author table
    __tablename__ = 'author'
    name = db.Column(db.String(80), primary_key=True)
    birthday = db.Column(db.String(), nullable=False, primary_key=True)
    nationality = db.Column(db.String(), nullable=False)
    bio = db.Column(db.String(), nullable=False)
    author_image = db.Column(db.String(), nullable=False)
    books_written = db.Column(db.String(), nullable=False)

class Publisher(db.Model):
    # publisher table
    __tablename__ = 'publisher'
    publisher = db.Column(db.String(80), primary_key=True)
    publisher_image = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=False)
    website = db.Column(db.String(), nullable=False)
    books_published = db.Column(db.String(), nullable=False)
    owner = db.Column(db.String(80), nullable=False)

db.drop_all()
db.create_all()
# End of models.py
